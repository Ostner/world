# World

App that shows the countries of the world with some detailed information.

![Demo](Resources/world.gif)

Apis used for this project:

- [Rest Countries](https://restcountries.eu)
- [GeoNames](http://www.geonames.org/)

The globe pictures are created by Ted Grajeda from the Noun Project.
