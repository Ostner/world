//
//  Country.swift
//  World
//
//  Created by Tobias Ostner on 4/26/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import CoreData
import UIKit

final class Country: NSManagedObject {
    @NSManaged private(set) var  name: String
    @NSManaged private(set) var population: Int32
    @NSManaged private(set) var capital: String
    @NSManaged private(set) var alpha2Code: String
    @NSManaged private(set) var flagURLString: String
    @NSManaged private var flagPNGData: Data?
    @NSManaged private(set) var region: Region
    @NSManaged private(set) var latitude: Double
    @NSManaged private(set) var longitude: Double
    @NSManaged private(set) var area: Double
    @NSManaged private(set) var languages: [String]
    @NSManaged private(set) var currencyNames: [String]
    @NSManaged private(set) var currencySymbols: [String]
    @NSManaged private(set) var gini: Float

    var flag: UIImage? {
        get {
            guard let data = flagPNGData else { return nil }
            return UIImage(data: data)
        }
        set {
            guard let image = newValue else { return }
            flagPNGData = UIImagePNGRepresentation(image)
        }
    }

    static func insert(
      into context: NSManagedObjectContext,
      json: [String:Any])
      -> Country?
    {
        guard let name = json["name"] as? String,
              let population = json["population"] as? Int32,
              let capital = json["capital"] as? String,
              let flag = json["flag"] as? String,
              let alpha2Code = json["alpha2Code"] as? String,
              let latlng = json["latlng"] as? [Double],
              let latitude = latlng.first,
              let longitude = latlng.last,
              let area = json["area"] as? Double,
              let languages = json["languages"] as? [[String:String]],
              let currencies = json["currencies"] as? [[String:String]],
              let gini = json["gini"] as? Float,
              let regionString = json["region"] as? String,
              !regionString.isEmpty
        else { return nil }
        let country: Country = context.insertObject()
        country.name = name
        country.population = population
        country.capital = capital
        country.flagURLString = flag
        country.latitude = latitude
        country.longitude = longitude
        country.area = area
        country.alpha2Code = alpha2Code
        country.languages = languages.map { $0["name"]! }
        country.currencyNames = currencies.map { $0["name"]! }
        country.currencySymbols = currencies.map { $0["symbol"]! }
        country.region = Region.findOrCreate(for: regionString, in: context)
        country.gini = gini
        return country
    }

    static var defaultSortDescriptors: [NSSortDescriptor] {
       let region = NSSortDescriptor(
         key: #keyPath(Country.region.name),
         ascending: true,
         selector: #selector(NSString.localizedCaseInsensitiveCompare(_:))
       )
        let name = NSSortDescriptor(
          key: #keyPath(Country.name),
          ascending: true,
          selector: #selector(NSString.localizedCaseInsensitiveCompare(_:))
        )
        return [region, name]
    }

}
