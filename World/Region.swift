//
//  Region.swift
//  World
//
//  Created by Tobias Ostner on 5/3/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import CoreData

final class Region: NSManagedObject {
    @NSManaged private(set) var name: String
    @NSManaged private(set) var countries: Set<Country>

    var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(
                  key: #keyPath(name),
                  ascending: true,
                  selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))]
    }

    static func findOrCreate(
      for name: String,
      in context: NSManagedObjectContext)
      -> Region
    {
        let predicate = NSPredicate(format: "%K == %@", #keyPath(Region.name), name)
        let region = findOrCreate(in: context, matching: predicate) {
            $0.name = name
        }
        return region
    }

    static func findOrCreate(
      in context: NSManagedObjectContext,
      matching predicate: NSPredicate,
      configure: (Region) -> ())
      -> Region
    {
        guard let region = findOrFetch(in: context, matching: predicate) else {
            let newRegion: Region = context.insertObject()
            configure(newRegion)
            return newRegion
        }
        return region
    }

    static func findOrFetch(
      in context: NSManagedObjectContext,
      matching predicate: NSPredicate)
      -> Region?
    {
        guard let region = materializedRegion(in: context, matching: predicate) else {
            let request = NSFetchRequest<Region>(entityName: entity().name!)
            request.predicate = predicate
            request.returnsObjectsAsFaults = false
            request.fetchLimit = 1
            let result = try! context.fetch(request)
            return result.first
        }
        return region
    }

    static func materializedRegion(
      in context: NSManagedObjectContext,
      matching predicate: NSPredicate)
      -> Region?
    {
        for object in context.registeredObjects where !object.isFault {
            guard let region = object as? Region,
                  predicate.evaluate(with: region)
            else { continue }
            return region
        }
        return nil
    }

}
