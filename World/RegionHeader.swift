//
//  RegionHeader.swift
//  World
//
//  Created by Tobias Ostner on 5/3/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import CoreData

final class RegionHeader: UICollectionReusableView {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var region: UIImageView!

    func configure(with sectionInfo: NSFetchedResultsSectionInfo) {
        let name = sectionInfo.name
        title.text = name
        region.image = UIImage(named: "\(name.lowercased())")
    }
}
