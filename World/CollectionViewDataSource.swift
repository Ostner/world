//
//  CollectionViewDataSource.swift
//  World
//
//  Created by Tobias Ostner on 4/29/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import CoreData

protocol CollectionViewDataSourceDelegate: class {
    associatedtype Object: NSFetchRequestResult
    associatedtype Cell: UICollectionViewCell
    associatedtype Header: UICollectionReusableView
    func configure(cell: Cell, for object: Object)
    func configure(header: Header, for sectionInfo: NSFetchedResultsSectionInfo)
}

final class CollectionViewDataSource<Delegate: CollectionViewDataSourceDelegate>:
  NSObject,
  UICollectionViewDataSource,
  NSFetchedResultsControllerDelegate
{
    typealias Object = Delegate.Object
    typealias Cell = Delegate.Cell
    typealias Header = Delegate.Header

    init(
      collectionView: UICollectionView,
      cellIdentifier: String,
      headerIdentifier: String,
      fetchedResultsController: NSFetchedResultsController<Object>,
      delegate: Delegate)
    {
        self.collectionView = collectionView
        self.cellIdentifier = cellIdentifier
        self.headerIdentifier = headerIdentifier
        self.fetchedResultsController = fetchedResultsController
        self.delegate = delegate
        super.init()
        fetchedResultsController.delegate = self
        try! fetchedResultsController.performFetch()
        collectionView.dataSource = self
        collectionView.reloadData()
    }

    subscript(key: IndexPath) -> Object {
        return fetchedResultsController.object(at: key)
    }

    // MARK: Private

    private let collectionView: UICollectionView
    private let cellIdentifier: String
    private let headerIdentifier: String
    private let fetchedResultsController: NSFetchedResultsController<Object>
    private let delegate: Delegate

    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    func collectionView(
      _ collectionView: UICollectionView,
      numberOfItemsInSection section: Int)
      -> Int
    {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }

    func collectionView(
      _ collectionView: UICollectionView,
      cellForItemAt indexPath: IndexPath)
      -> UICollectionViewCell
    {
        let object = fetchedResultsController.object(at: indexPath)
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                            for: indexPath) as? Cell
        else { fatalError("Wrong cell type at \(indexPath)") }
        delegate.configure(cell: cell, for: object)
        return cell
    }

    func collectionView(
      _ collectionView: UICollectionView,
      viewForSupplementaryElementOfKind kind: String,
      at indexPath: IndexPath)
      -> UICollectionReusableView
    {
        let header = collectionView.dequeueReusableSupplementaryView(
          ofKind: kind,
          withReuseIdentifier: headerIdentifier,
          for: indexPath
        ) as! Header
        let sectionInfo = fetchedResultsController.sections![indexPath.section]
        delegate.configure(header: header, for: sectionInfo)
        return header
    }

    // MARK: NSFetchedResultsControllerDelegate

    private var insertedItems: [IndexPath] = []
    private var deletedItems: [IndexPath] = []
    private var insertedSections = IndexSet()
    private var deletedSections = IndexSet()
    private var updatedItems: [IndexPath] = []

    func controllerWillChangeContent(
      _ controller: NSFetchedResultsController<NSFetchRequestResult>)
    {
        insertedItems = []
        deletedItems = []
        insertedSections.removeAll()
        deletedSections.removeAll()
        updatedItems = []
    }

    func controller(
      _ : NSFetchedResultsController<NSFetchRequestResult>,
      didChange object: Any,
      at indexPath: IndexPath?,
      for type: NSFetchedResultsChangeType,
      newIndexPath: IndexPath?)
    {
        switch type {
        case .insert:
            insertedItems.append(newIndexPath!)
        case .update:
            updatedItems.append(indexPath!)
        case .move:
            fatalError("not implemented yet")
        case .delete:
            deletedItems.append(indexPath!)
        }
    }

    func controller(
      _ : NSFetchedResultsController<NSFetchRequestResult>,
      didChange sectionInfo: NSFetchedResultsSectionInfo,
      atSectionIndex sectionIndex: Int,
      for type: NSFetchedResultsChangeType)
    {
        switch type {
        case .insert:
            insertedSections.insert(sectionIndex)
        case .delete:
            deletedSections.insert(sectionIndex)
        default:
            fatalError("not implemented yet")
        }

    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView.performBatchUpdates(
          {
              print("perform batch updates")
              self.collectionView.insertSections(self.insertedSections)
              self.collectionView.insertItems(at: self.insertedItems)
              self.collectionView.reloadItems(at: self.updatedItems)
              self.collectionView.deleteItems(at: self.deletedItems)
              self.collectionView.deleteSections(self.deletedSections)
          },
          completion: nil)
    }
}
