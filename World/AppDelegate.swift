//
//  AppDelegate.swift
//  World
//
//  Created by Tobias Ostner on 4/26/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var persistentContainer: NSPersistentContainer!

    func application(
      _ application: UIApplication,
      didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)
      -> Bool {
        createWorldContainer {
            container in
            self.persistentContainer = container
            guard let nav = self.window?.rootViewController as? UINavigationController,
                  let vc = nav.topViewController as? CountriesViewController
            else { fatalError("Wrong view controllers") }
            vc.managedContext = self.persistentContainer.viewContext
        }
        let navbarAppearance = UINavigationBar.appearance()
        navbarAppearance.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Quicksand", size: 24)!]
        navbarAppearance.tintColor = .black
        return true
    }

}
