//
//  Webservice.swift
//  World
//
//  Created by Tobias Ostner on 4/30/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

let flagBaseURL = URL(string: "http://www.geonames.org/flags/x/")
let allCountriesURL = URL(string:  "https://restcountries.eu/rest/v2/all")!

struct Resource<T> {
    let url: URL
    let transform: (Data) -> T?
}

final class Webservice {
    static func load<T>(_ resource: Resource<T>, completion: @escaping (T?) -> Void) {
        URLSession.shared.dataTask(with: resource.url) {
            data, _, _ in
            completion(data.flatMap(resource.transform))
        }.resume()
    }
}
