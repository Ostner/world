//
//  CountryCell.swift
//  World
//
//  Created by Tobias Ostner on 4/26/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

final class CountryCell: UICollectionViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var flagImageView: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    func configure(with country: Country) {
        name.text = country.name
        if let flag = country.flag {
            flagImageView.image = flag
        } else {
            loadFlag(for: country)
        }
    }

    override func prepareForReuse() {
        flagImageView.image = nil
        name.text = nil
    }

    // MARK: Private

    private func setup() {
        backgroundView = UIImageView()
    }

    private func loadFlag(for country: Country) {
        print("loading flag")
        let code = country.alpha2Code.lowercased()
        let url = URL(string: "\(code).gif", relativeTo: flagBaseURL)!
        let resource = Resource<UIImage>(url: url) { UIImage(data: $0) }
        Webservice.load(resource) {
            image in
            guard let image = image else { print("image is nil"); return }
            country.managedObjectContext?.performChanges {
                country.flag = image
                self.flagImageView.image = image
            }
        }

    }

}
