//
//  CountriesViewController.swift
//  World
//
//  Created by Tobias Ostner on 4/26/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import CoreData

final class CountriesViewController:
  UICollectionViewController,
  CollectionViewDataSourceDelegate,
  UISearchResultsUpdating,
  UISearchControllerDelegate
{

    var managedContext: NSManagedObjectContext! {
        didSet {
            setupCollectionView()
            setupSearchController()
        }
    }

    let allCountries: Resource<[[String:Any]]> = {
        return Resource(url: allCountriesURL) {
            data in
            guard let deserialized = try? JSONSerialization.jsonObject(with: data, options: [])
            else { return nil }
            return deserialized as? [[String:Any]]
        }
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        let rc = UIRefreshControl()
        rc.attributedTitle =  NSAttributedString(
          string: "Load World Countries",
          attributes: [:])
        rc.addTarget(
          self,
          action: #selector(loadCountries),
          for: .valueChanged)
        collectionView?.refreshControl = rc
        let layout = collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionHeadersPinToVisibleBounds = true
        layout.itemSize = CGSize(width: collectionView!.bounds.width, height: 130)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? CountryViewController
        else { fatalError("Wrong view controller for segue \(segue.identifier)") }
        let indexPath = collectionView!.indexPathsForSelectedItems!.first!
        destination.country = dataSource[indexPath]
    }

    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if case .motionShake = motion {
            resetDatabase()
            collectionView?.reloadData()
        }
    }

    // MARK: Actions

    func loadCountries(sender: UIRefreshControl) {
        print("fetch all countries")
        Webservice.load(allCountries) {
            json in
            guard let json = json else { return }
            DispatchQueue.main.async {
                self.managedContext.performChanges {
                    json.forEach { _ = Country.insert(into: self.managedContext, json: $0) }
                }
                sender.endRefreshing()
            }
        }
    }

    func resetDatabase() {
        let request = NSFetchRequest<Country>(entityName: "Country")
        let result = try! managedContext.fetch(request)
        managedContext.performChanges {
            result.forEach { self.managedContext.delete($0) }
        }
    }

    // MARK: Private

    private var dataSource: CollectionViewDataSource<CountriesViewController>!
    private var searchController: UISearchController!
    private var fetchedResultsController: NSFetchedResultsController<Country>!

    private func setupCollectionView() {
        let request = NSFetchRequest<Country>(entityName: "Country")
        request.sortDescriptors = Country.defaultSortDescriptors
        request.returnsObjectsAsFaults = false
        fetchedResultsController = NSFetchedResultsController(
          fetchRequest: request,
          managedObjectContext: managedContext,
          sectionNameKeyPath: #keyPath(Country.region.name),
          cacheName: nil)
        dataSource = CollectionViewDataSource(
          collectionView: collectionView!,
          cellIdentifier: "CountryCell",
          headerIdentifier: "RegionHeader",
          fetchedResultsController: fetchedResultsController,
          delegate: self)
    }

    private func setupSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.delegate = self
        navigationItem.titleView = searchController.searchBar
    }


    // MARK: CollectionViewDataSourceDelegate

    func configure(cell: CountryCell, for object: Country) {
        cell.configure(with: object)
    }

    func configure(header: RegionHeader, for sectionInfo: NSFetchedResultsSectionInfo) {
        header.configure(with: sectionInfo)
    }

    // MARK: UISearchResultsUpdating

    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else { return }
        let predicate = searchText.isEmpty ?
          nil :
          NSPredicate(format: "name BEGINSWITH[cd] %@", searchText)
        fetchedResultsController.fetchRequest.predicate = predicate
        try! fetchedResultsController.performFetch()
        collectionView?.reloadData()
    }

    // MARK: UISearchcontrollerdelegate

    func didDismissSearchController(_ searchController: UISearchController) {
        fetchedResultsController.fetchRequest.predicate = nil
        try! fetchedResultsController.performFetch()
        collectionView?.reloadData()
    }
}
