//
//  WorldCoreDataStack.swift
//  World
//
//  Created by Tobias Ostner on 4/28/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import CoreData

func createWorldContainer(completion: @escaping (NSPersistentContainer) -> ()) {
    let container = NSPersistentContainer(name: "World")
    container.loadPersistentStores {
        _, error in
        guard error == nil else { fatalError("Failed to load store: \(error)") }
        DispatchQueue.main.async { completion(container) }
    }
}
