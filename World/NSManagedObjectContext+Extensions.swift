//
//  NSManagedObjectContext+Extensions.swift
//  World
//
//  Created by Tobias Ostner on 4/28/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import CoreData

extension NSManagedObjectContext {
    func insertObject<T: NSManagedObject>() -> T {
        guard let entityName = T.entity().name,
              let obj = NSEntityDescription.insertNewObject(forEntityName: entityName, into: self) as? T
        else { fatalError("Wrong object type") }
        return obj
    }

    func saveOrRollback() -> Bool {
        do {
            print("save to database")
            try save()
            return true
        } catch {
            rollback()
            return false
        }
    }

    func performChanges(block: @escaping () -> Void) {
        perform {
            block()
            _ = self.saveOrRollback()
        }
    }
}
