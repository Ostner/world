//
//  CountryViewController.swift
//  World
//
//  Created by Tobias Ostner on 4/26/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import MapKit

final class CountryViewController: UIViewController {

    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var capital: UILabel!
    @IBOutlet weak var area: UILabel!
    @IBOutlet weak var population: UILabel!
    @IBOutlet weak var languages: UILabel!
    @IBOutlet weak var currencies: UILabel!
    @IBOutlet weak var gini: UILabel!

    var country: Country!


    override func viewDidLoad() {
        super.viewDidLoad()
        title = country.name
        setupMapView()
        capital.text = country.capital
        area.text = areaString
        population.text = populationString
        languages.text = country.languages.joined(separator: ", ")
        currencies.text = currenciesString
        gini.text = String(country.gini)
    }

    // MARK: Private

    private func setupMapView() {
        let center = CLLocationCoordinate2D(
          latitude: country.latitude,
          longitude: country.longitude
        )
        let areaSquareRootMeters = sqrt(country.area) * 1000
        let region = MKCoordinateRegionMakeWithDistance(
          center,
          areaSquareRootMeters * 1.5,
          areaSquareRootMeters * 1.5
        )
        map.region = region
    }

    private var areaString: String {
        let area = Measurement(
          value: country.area,
          unit: UnitArea.squareKilometers
        )
        let formatter = MeasurementFormatter()
        return formatter.string(from: area)
    }

    private var populationString: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        let populationNum = NSNumber(value: country.population)
        return formatter.string(from: populationNum)!
    }

    private var currenciesString: String {
        var result = ""
        for (idx, name) in country.currencyNames.enumerated() {
            result.append("\(name) (\(country.currencySymbols[idx]))")
        }
        return result
    }

}
